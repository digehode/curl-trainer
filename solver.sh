#!/bin/bash
rm cookieja.r
C="curl http://localhost:8090"
C_SSL="curl https://localhost:8091"

${C}/begin.xml
${C}/a.a -X POST
${C}/data.php?age=45
${C}/index.html -0
${C}/index.html -A "Zombobrowser 1.0, Awesome Enabled"
${C}/zip -b sugar=AucVIAdE


i=$(${C}/zip -b cookieja.r -c cookieja.r -s)

while [[ $i == Not* ]]
do
    i=$(${C}/zip -b cookieja.r -c cookieja.r -s)
    echo -n "."
done
echo "Completed cookie pingpong"

${C}/whoami.php -u joe:bloggs

uname_pw=$(${C}/instructions -s|cut -d " " -f 30|base64 -d)
${C}/whoami.php -u $uname_pw
${C}/gzipme --compressed

data=$(${C}/zipgme -s -H "Accept-Encoding: deflate, gzip"|tail -c +33|python -c "import zlib, sys; print zlib.decompress(sys.stdin.read())")
echo $data
${C}$data
${C_SSL}/shizzle.pl -k

#OCR:
captcha=$(${C}/cap.png -s | convert png:- pnm:-|gocr -a 80 -)
echo OCR Captcha gives $captcha
${C}/totallynotarobot.php -X POST -d cap=$captcha

${C}/instructions
