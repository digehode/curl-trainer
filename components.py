class PathSpec(object):
    """ Defines a path specification. Given a path, sets up how to respond """
    def __init__(self,path,content_type="text/plain", content=None, raw=False, doCookies=lambda x: True):
        self.path=path
        self.content_type=content_type
        self.content=content
        self.raw=raw
        self.doCookies=doCookies
class State(object):
    """
    """
    
    def __init__(self, title="Unnamed challenge",instruction="Tell the user how to move on", conditions=[],hint="A hint goes here!", pathSpecs={}):
        self._title=title
        """
        
        Arguments:
        - `instruction`:
        - `conditions`:
        - `hint`:
        - `pathSpecs`:
        """
        self._instruction = instruction
        self._conditions = conditions
        self._hint = hint
        self._pathSpecs=pathSpecs
    
    def hasPath(self,p):
        return self._pathSpecs.has_key(p)
    def pathSpec(self,p):
    
        return self._pathSpecs[p]
    def data(self):
        return self._data
    def doCookies(self,serv):
        return self.pathSpec(serv.path).doCookies(serv)
    def __str__(self):
        return self._title
