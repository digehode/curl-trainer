import Image, StringIO
import ImageFont, ImageDraw
import oddHash, random

def makeCaptcha():
    data=StringIO.StringIO()
    r=random.Random()
    h=oddHash.oddHash( r.randint(0,1000), map=oddHash.ocrMap)
    i=Image.new("RGB",(600,150), (255,255,255))
    draw = ImageDraw.Draw(i)
    font = ImageFont.truetype("tahoma.ttf", 94)
    draw.text((10, 25) , h, font=font, fill=(0,0,0))
    i.save(data,format="PNG")
    return h,data.getvalue()

if __name__=="__main__":
    a=makeCaptcha()
    print a[1]
