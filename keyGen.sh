#See http://www.akadia.com/services/ssh_test_certificate.html

#Generate private key
openssl genrsa -des3 -out server.key 1024

#Generate certificate signing request (CSR)
openssl req -new -key server.key -out server.csr

# #Remove passphrase from key
cp server.key server.key.org
openssl rsa -in server.key.org -out server.key

#Generate self-signed certificate
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt



# ##From somewhere else...
# #making pem keys for client/server
openssl rsa -in server.key -text > private.pem
openssl x509 -inform PEM -in server.crt > public.pem
