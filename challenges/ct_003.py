from components import PathSpec, State
def construct(config):
        return State(title="HTTP Version",instruction="I'm fed up of all this modern stuff.  Make me an old fashioned request. ", conditions=[lambda x: x.request_version=="HTTP/1.0"],hint="Try using HTTP v. 1.0...")
