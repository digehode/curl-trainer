from components import PathSpec, State
def construct(config):
            return State(title="GET parameter",instruction="Request the page /data.php using the GET method passing in the parameter 'age' with a value of 45. ",
                         conditions=[lambda x: x.path.startswith("/data.php"),
                                     lambda x: x.command=="GET",
                                     lambda x: x.headers["GET_data"].has_key("age") and x.headers["GET_data"]["age"]==["45"]],
                         hint="Where to GET parameters go?")
