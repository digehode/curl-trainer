from components import PathSpec, State
def construct(config):
    return State(title="Cookie basics",instruction="Request the page /zip, sending the cookie called 'sugar' with the value 'AucVIAdE'",
                 conditions=[lambda x: x.path.startswith("/zip"),
                             lambda x: x.getCookie("sugar")=="AucVIAdE"],
                 hint="man curl.")

