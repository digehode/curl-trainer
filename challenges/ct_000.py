from components import PathSpec, State
def construct(config):
    return State(title="Simple request",instruction="Make a simple request for the page called 'begin.xml' in the server root", conditions=[lambda x: x.path=="/begin.xml"],hint="man curl")
