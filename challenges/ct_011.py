from components import PathSpec, State
def construct(config=None):
        return State(title="HTTPS basic",instruction="For security purposes, please request the next page (/shizzle.pl) using SSL.  The server accepts secure connections on port %d"%(config["PORT"]+1), conditions=[lambda x: x.path=="/shizzle.pl", lambda x: x.ssl],hint="Trouble getting curl to realise it's HTTPS?  How does a BROWSER know this?  Or maybe you sussed that and got stuck because you can't trust my certificate?  Maybe you need to ask curl to ignore the risk of self-signed certificates...")
