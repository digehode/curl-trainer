from components import PathSpec, State
import oddHash
def tmpCookieSet(x):            
    v=x.getCookie("sugar")
    if v=="dXVLqElM":
        #Success!
        #Now make it stick...
        pass #by doing nothing...
    elif v!=None:
        x.setCookie("sugar",oddHash.oddHash(v))
    else:
        x.setCookie("sugar","rush")

def construct(config):
        return State(title="Cookie jar",
            instruction="Request the page /zip. It will send you a coookie. Send it back. Do it again.  And again. Keep going until I say otherwise.",
            conditions=[lambda x: x.path.startswith("/zip"),
                        lambda x: x.getCookie("sugar")=="Z3wmBUHX"],
            hint="You should be using a cookie jar. I hope you aren't making a note and setting it by hand... Also, curl doesn't assume that it should get cookies out of the jar unless you tell it that too.",
            pathSpecs={"/zip":PathSpec("/zip", doCookies=tmpCookieSet)}
            
        )
    
    
