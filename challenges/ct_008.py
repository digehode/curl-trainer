from components import PathSpec, State
import email.base64mime

def construct(config):

    return State(title="Decoding basicauth",instruction="Request the page /whoami.php again.  You need to log in using captured credentials, though.  I nabbed a header for you and it had the base64 encoded username/password: c2Nhcnk6YnVubnk=",
                 conditions=[lambda x: x.headers.has_key("Authorization") and x.headers["Authorization"].find(email.base64mime.encode("scary:bunny").strip())>-1,
                             lambda x: x.path=="/whoami.php"],
                 hint="This is known as 'basic' authorization. You could just work out the header format and send the encoded stuff "+\
                 "back without knowing the username or password, but why not see if you can decode it and actually use the username and password instead?")
