from components import PathSpec, State
def construct(config):
        return State(title="User agents",instruction="This stage best viewed with 'zombobrowser'. ", conditions=[lambda x: x.headers["User-Agent"].upper().find("ZOMBOBROWSER")>-1],hint="How does a server know what browser you use?")
