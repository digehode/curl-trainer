from components import PathSpec, State
import email.base64mime

def construct(config):
    return State(title="basicauth username and password", instruction="Request the page /whoami.php as the user 'joe' with password 'bloggs'",
                 conditions=[lambda x: x.headers.has_key("Authorization") and x.headers["Authorization"].find(email.base64mime.encode("joe:bloggs").strip())>-1,
                             lambda x: x.path=="/whoami.php"],
                 hint="man curl. Should be easy to find.")
