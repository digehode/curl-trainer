from components import PathSpec, State
import struct, zlib
def construct(config):

    return State(title="Monkeying around with compressed data",
        instruction="OK, so you can get curl to send you gzipped data and you can have it decompress automatically.  Now ask for the page /zipgme, with compression.  Don't let curl automatically decompress the data.  Strip the first 32 bytes off it, then uncompress it (zlib/gzip) to find the URL of the page you need to request to move on.",
        conditions=[lambda x: x.path=="/sacrosanct"],
        hint="Instead of just switching on compression, you need to find a way to get the server to compress the data without asking curl to decompress.  gunzip won't do it without headers attached, so you will need to find a way of adding the headers or using zlib itself.  I suggest python... If you are really stuck, unrot13 the following for a python commandline prog you can pipe stuff through: clguba -p \"vzcbeg myvo, flf; cevag myvo.qrpbzcerff(flf.fgqva.ernq()[32:])\"",
        pathSpecs={"/zipgme":PathSpec("/zipgme",content=struct.pack("B"*32,*range(32))+zlib.compress("/sacrosanct"),raw=True)}
    )
