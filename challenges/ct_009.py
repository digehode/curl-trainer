from components import PathSpec, State

def construct(config):
        return State(title="Compressed responses",
            instruction="Sometimes, you want to minimise traffic using compression.  Make your next request for the page /gzipme to be returned with compression.",
            conditions=[lambda x: x.gzipped,
                        lambda x: x.path=="/gzipme"],
            hint="There's a simple switch for curl that will enable compression. You won't notice the difference because curl will decode it for you, but you can dump the headers if you're interested.")
