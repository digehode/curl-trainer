from components import PathSpec, State
import imgen
def construct(config=None):
    capText,capIm=imgen.makeCaptcha()
    
    return State(title="CAPTCHA",
                 instruction="There is a captcha image at /cap.png. If you can decode it, you need to send the response as a POST variable named 'cap' to /totallnotarobot.php",
                 conditions=[lambda x: x.path=="/totallynotarobot.php",
                             lambda x: x.command=="POST",
                             lambda x: x.headers["POST_data"].has_key("cap"),
                             lambda x: x.headers["POST_data"].has_key("cap") and x.headers["POST_data"]["cap"][0]==capText
                 ],
                 
                 hint="You could just read it, but you *should* be able to pipe the output into an OCR package (such as gocr) and return it programmatically...",
                 pathSpecs={"/cap.png":PathSpec("/cap.png",content_type="image/png", content=capIm)}
    )

