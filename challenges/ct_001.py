from components import PathSpec, State
def construct(config):
    return State(title="POST request",instruction="Make a POST request for /a.a", conditions=[lambda x: x.path=="/a.a", lambda x: x.command=="POST"],hint="man curl... still")
