#Hash updated to use md5 because the _builtin_ hash() is architecture dependant, it seems
import md5


map=[str(i) for i in range(10)]+[chr(i+ord("a")) for i in range(26)]+[chr(i+ord("A")) for i in range(26)]


ocrMap=[chr(i+ord("a")) for i in range(26)]+[chr(i+ord("A")) for i in range(26)]
def oddHash(t, length=8, map=map):
    """ Hashes will be of a given size and be represented as alphanumerics """
    s= md5.md5(str(t)).digest()
    n=sum([(i**20)*ord(s[i]) for i in range(len(s))])

    
    out=""
    for i in range(length):
        c=n&255
        n/=128
        n+=c*2**64
        out=out+map[c % len(map)]
    return out


if __name__== '__main__':
    val= oddHash("rush")
    for i in range(50):
        print val
        val=oddHash(val)

