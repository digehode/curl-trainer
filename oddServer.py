import oddHash
import Cookie, cgi
import socket
import BaseHTTPServer
import SocketServer
import zlib
import ssl
import threading
import imgen
from components import PathSpec, State
import os,sys
import inspect
#The SSL version will always be PORT+1
PORT=8090

def slyOut(t):
    print "Sly:",t
    return True

class GameStateMachine(object):
    """
    """

    def __init__(self,challengeDir):
        self.states=[]
        self.state=0

        
        files=os.listdir(challengeDir)
        mods=[]
        for f in files:
            if f.endswith(".py") and f.startswith("ct_"):
                mods.append(f)
        mods.sort()
        sys.path.insert(0, challengeDir)
        for m in mods:
            name = os.path.splitext(m)[0]
            print "Loading:",name,
            
            mod = __import__(name)            
            s=mod.construct(config={"PORT":PORT})
            self.states.append(s)
            print s

            
            
                
    def not__init__(self):
        """
        """

        
    def instruction(self):
        if self.state<len(self.states):
            return self.states[self.state]._instruction
        else:
            return "You have completed %d/%d steps. Congratulations."%(len(self.states),len(self.states))

    def hint(self):
        if self.state<len(self.states):
            return self.states[self.state]._hint
        else:
            return "You finished.  Nothing more to do."

    def check(self,server):
        
        count=0
        for c in self.states[self.state]._conditions:
            if c(server):
                print "Condition met"
                count+=1
            else:
                print "Condition failed"
        
        return count== len(self.states[self.state]._conditions),count,len(self.states[self.state]._conditions)
    
    def next(self):
        self.state+=1
    def done(self):
        return not self.state<len(self.states)
    def current(self):
        if self.state<len(self.states):
            return self.states[self.state]
        return None
s=GameStateMachine("./challenges/")

class MyServer(BaseHTTPServer.BaseHTTPRequestHandler):
    def send(self,text):
        if self.gzipped:
            text=zlib.compress(text+"\n")
        self.write(text)
    def do_GET(self ):
        print "GET"
        headers=self.getHeaders()

        #self.debug()
        self.send_response(200)
        
        if s.current()!=None and s.current().hasPath(self.path):
            self.send_header('Content-type', s.current().pathSpec(self.path).content_type )
        else:
            #Defalt to text/plain
            self.send_header('Content-type', 'text/plain')

        if s.current()!=None and s.current().hasPath(self.path):
            s.current().doCookies(self)

        if self.headers.has_key("Accept-Encoding") and self.headers["Accept-Encoding"].upper().find("GZIP")>-1:
            self.send_header("Content-Encoding","gzip")
            self.gzipped=True
        else:
            self.gzipped=False
        self.end_headers()                

        if self.path=="/instructions":
            self.send(s.instruction())
            return
        elif self.path=="/hint":
            self.send(s.hint())
            return
        if s.current()!=None and s.current().hasPath(self.path) and s.current().pathSpec(self.path).content!=None:
            ps=s.current().pathSpec(self.path)
            if ps.raw:
                self.write(ps.content)
            else:
                self.send(ps.content)
            return

        if not s.done():
            correct= s.check(self)
            if correct[0]:
                self.send("Well done. You completed the challenge %s.\n"%s.current())
                s.next()
                return
            elif correct[1]>0:
                self.send("Not quite.  You got %d of %d conditions correct"%(correct[1],correct[2]))
                return
            else:
                if self.path=="/":
                    self.send("To play, request the /instructions page.  If you get stuck, ask for the /hint page.")
                    return

        self.send("Sorry, not a clue what you mean.\n")
    def write(self,s):
        self.wfile.write(s)
        self.wfile.write("\n\n")
    def getHeaders(self):

        self.headers=dict([[i[:i.find(":")].strip(),i[i.find(":")+1:].strip()] for i in self.headers.headers])
        #Now push content in, too
        if self.headers.has_key("Content-Length"):
            self.headers["Content"]=self.rfile.read(int(self.headers["Content-Length"]))
        #And any parameters from the URL
        q=cgi.urlparse.urlparse(self.path)
        self.headers["GET_data"]= cgi.urlparse.parse_qs(q.query)
        
        self.ssl=self.server.isSSL()
        #And now POST data items
        self.headers["POST_data"]={} #So we can still check it even if there are no values...
        if self.headers.has_key("Content"):
            #print self.headers["Content"]
            q=cgi.urlparse.urlparse(self.headers["Content"])
            #print q,cgi.urlparse.parse_qs(q.path) #Because it's not preceded by a path...
            self.headers["POST_data"]= cgi.urlparse.parse_qs(q.path)
            
        return self.headers
    def do_POST(self):
        self.do_GET()
    def do_HEAD(self):
        print "HEAD"
    def debug(self):
        items=["client_address","server","command","path","request_version"]
        for i in items:
            print "\t%s: %s"%(i,str(self.__dict__[i]))

        for i in self.headers:
            print i,":\t" ,self.headers[i]
        #print self.headers.getheader("Content-Length")

    def setCookie(self, k, v):
        c = Cookie.SimpleCookie()
        c[k] = v
        self.send_header('Set-Cookie', c.output(header=''))
    def getCookie(self,k):
        if "Cookie" in self.headers:
            c = Cookie.SimpleCookie(self.headers["Cookie"])
            if c.has_key(k):
                return c[k].value
        return None
        
class MyTCPServer(SocketServer.TCPServer):
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)
    def isSSL(self):
        return False

class MyTCPServerSSL(SocketServer.TCPServer):

    def server_bind(self):
        print "SSL bind"
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)
    def get_request(self):
        print "SSL request..."
        (socket, addr) = SocketServer.TCPServer.get_request(self)
        return (ssl.wrap_socket(socket, server_side=True, certfile="server.crt", keyfile="server.key"),
                addr)
    def isSSL(self):
        return True



Handler=MyServer
httpdSSL=MyTCPServerSSL(("",PORT+1),Handler)
httpd=MyTCPServer(("",PORT),Handler)
print "Serving in port %d"%PORT

t = threading.Thread(target=httpdSSL.serve_forever)
t.start()
t2=threading.Thread(target=httpd.serve_forever)
t2.start()
u=""
while u not in ['Q', 'q']:
    print "Q to quit"
    u=raw_input()
print "Shutting down HTTP listener..."
httpd.shutdown()
print "Shutting down HTTPS listener..."
httpdSSL.shutdown()
print "Waiting for threads to stop"
t.join()
t2.join()
print "end?"
